import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { IssaApp } from './app.component';
//import { HomePage } from '../pages/home/home';
//import { WelcomePage } from '../pages/welcome/welcome';
//import { WhatIsPage } from '../pages/what-is/what-is';
//import { SignupPage } from '../pages/signup/signup';
//import { LoginPage } from '../pages/login/login';

@NgModule({
  declarations: [
    IssaApp,
//    WelcomePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(IssaApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    IssaApp,
//    WelcomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
