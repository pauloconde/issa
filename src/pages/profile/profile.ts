import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

  closedCases(){
    console.log('Casos cerrados');
    this.navCtrl.push('CasesPage', {status: 'closed', title: 'Cerrados'});
  }

  editProfile(){
    console.log('Editar perfil');
  }

  settings(){
    console.log('Configuración');
  }

  logout(){
    console.log('Cerrando sesión');
    this.navCtrl.setRoot('WelcomePage');
  }
}
