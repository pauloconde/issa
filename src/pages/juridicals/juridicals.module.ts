import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { JuridicalsPage } from './juridicals';

@NgModule({
  declarations: [
    JuridicalsPage,
  ],
  imports: [
    IonicPageModule.forChild(JuridicalsPage),
  ],
})
export class JuridicalsPageModule {}
