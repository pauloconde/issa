import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CasePhotoPage } from './case-photo';

@NgModule({
  declarations: [
    CasePhotoPage,
  ],
  imports: [
    IonicPageModule.forChild(CasePhotoPage),
  ],
})
export class CasePhotoPageModule {}
