import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CaseAskPage } from './case-ask';

@NgModule({
  declarations: [
    CaseAskPage,
  ],
  imports: [
    IonicPageModule.forChild(CaseAskPage),
  ],
})
export class CaseAskPageModule {}
