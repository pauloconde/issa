import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-case-ask',
  templateUrl: 'case-ask.html',
})
export class CaseAskPage {

  public caso: any;
  public description: string;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.caso = navParams.get('case');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CaseAskPage', this.caso);
  }

  selectImage(type){
    if(type=='J') return 'assets/imgs/juridical-icon.png';
    return 'assets/imgs/accounting-icon.png'
  }

  position(){
    if(this.caso.type == 'C') return 'Contador';
    return 'Abogado';
  }

  addAsk(){
    console.log('Agregando pregunta al caso', this.caso);
    this.navCtrl.pop();
  }
}
