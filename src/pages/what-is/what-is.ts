import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

/**
 * The WhatIs Page is a splash page that quickly describes the app,
 * and then directs the user to create an account or log in.
 * If you'd like to immediately put the user onto a login/signup page,
 * we recommend not using the WhatIs page.
*/
@IonicPage()
@Component({
  selector: 'page-what-is',
  templateUrl: 'what-is.html'
})
export class WhatIsPage {

  constructor(public navCtrl: NavController) { }

  login() {
    this.navCtrl.push('LoginPage');
  }

  signup() {
    this.navCtrl.push('SignupPage');
  }
}
