import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { WhatIsPage } from './what-is';

@NgModule({
  declarations: [
    WhatIsPage,
  ],
  imports: [
    IonicPageModule.forChild(WhatIsPage),
  ],
  exports: [
    WhatIsPage
  ]
})
export class WhatIsPageModule { }
