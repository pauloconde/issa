import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

//import { LoginPage } from '../login/login';
//import { WhatIsPage } from '../what-is/what-is';


@IonicPage()
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html'
})
export class WelcomePage {

  constructor(public navCtrl: NavController) { }

  login() {
    this.navCtrl.push('LoginPage');
  }

  info() {
    this.navCtrl.push('WhatIsPage');
  }
}
