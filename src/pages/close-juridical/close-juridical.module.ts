import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CloseJuridicalPage } from './close-juridical';

@NgModule({
  declarations: [
    CloseJuridicalPage,
  ],
  imports: [
    IonicPageModule.forChild(CloseJuridicalPage),
  ],
})
export class CloseJuridicalPageModule {}
