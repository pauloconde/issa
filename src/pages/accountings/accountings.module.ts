import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AccountingsPage } from './accountings';

@NgModule({
  declarations: [
    AccountingsPage,
  ],
  imports: [
    IonicPageModule.forChild(AccountingsPage),
  ],
})
export class AccountingsPageModule {}
