import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CloseAccountingPage } from './close-accounting';

@NgModule({
  declarations: [
    CloseAccountingPage,
  ],
  imports: [
    IonicPageModule.forChild(CloseAccountingPage),
  ],
})
export class CloseAccountingPageModule {}
