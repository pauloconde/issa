import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-new-juridical',
  templateUrl: 'new-juridical.html',
})
export class NewJuridicalPage {

  public case: { date: string, number: string, counterpath: string, title: string, description: string };
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.case  = { date: '', number: '', counterpath: '', title: '', description: ''};
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NewJuridicalPage');
  }

  doCase(){
    this.navCtrl.push('OkJuridicalPage');
    console.log('Crear caso');
  }

  goHome(){
    this.navCtrl.push('HomePage');
    console.log('Ir a inicio');
  }

  addAudio(){
    this.navCtrl.push('AudioJuridicalPage');
    console.log('Agregar audio');
  }
}
