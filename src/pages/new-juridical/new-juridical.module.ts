import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewJuridicalPage } from './new-juridical';

@NgModule({
  declarations: [
    NewJuridicalPage,
  ],
  imports: [
    IonicPageModule.forChild(NewJuridicalPage),
  ],
})
export class NewJuridicalPageModule {}
