import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-ok-juridical',
  templateUrl: 'ok-juridical.html',
})
export class OkJuridicalPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OkJuridicalPage');
  }

  goHome(){
    this.navCtrl.push('HomePage');
    console.log('Ir a inicio');
  }

  viewOpen(){
    this.navCtrl.push('CasesPage', { status: 'open', title: 'Abiertos' });
    console.log('Ver casos abiertos');
  }
}
