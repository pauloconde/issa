import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OkJuridicalPage } from './ok-juridical';

@NgModule({
  declarations: [
    OkJuridicalPage,
  ],
  imports: [
    IonicPageModule.forChild(OkJuridicalPage),
  ],
})
export class OkJuridicalPageModule {}
