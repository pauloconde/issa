import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewAccountingPage } from './new-accounting';

@NgModule({
  declarations: [
    NewAccountingPage,
  ],
  imports: [
    IonicPageModule.forChild(NewAccountingPage),
  ],
})
export class NewAccountingPageModule {}
