import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-new-accounting',
  templateUrl: 'new-accounting.html',
})
export class NewAccountingPage {

  public case: { date: string, number: string, counterpath: string, title: string, description: string };
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.case  = { date: '', number: '', counterpath: '', title: '', description: ''};
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NewAccountingPage');
  }

  doCase(){
    this.navCtrl.push('OkAccountingPage');
    console.log('Crear caso');
  }

  goHome(){
    this.navCtrl.push('HomePage');
    console.log('Ir a inicio');
  }

  addAudio(){
    this.navCtrl.push('AudioJuridicalPage');
    console.log('Agregar audio');
  }
}
