import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CaseFilePage } from './case-file';

@NgModule({
  declarations: [
    CaseFilePage,
  ],
  imports: [
    IonicPageModule.forChild(CaseFilePage),
  ],
})
export class CaseFilePageModule {}
