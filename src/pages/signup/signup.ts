import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { LoginPage } from '../login/login';


@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {
  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  account: { firstname: string, lastname: string, email: string, phone: string, address1: string, address2: string, city: string, state: string, county: string, zip: string, borndate: string, gender: string } = {
    firstname: '', lastname: '', email: '', phone: '', address1: '', address2: '', city: '', state: '', county: '', zip: '', borndate: '', gender: ''};

  constructor(public navCtrl: NavController) {

  }

  doSignup() {


      this.navCtrl.push(HomePage);

  }

  doLogin(){
    this.navCtrl.push(LoginPage);
  }
}
