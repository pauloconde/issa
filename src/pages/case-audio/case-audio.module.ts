import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CaseAudioPage } from './case-audio';

@NgModule({
  declarations: [
    CaseAudioPage,
  ],
  imports: [
    IonicPageModule.forChild(CaseAudioPage),
  ],
})
export class CaseAudioPageModule {}
