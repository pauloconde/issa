import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-cases',
  templateUrl: 'cases.html',
})
export class CasesPage {

  public status: string;
  public title: string;
  public type: string;
  public cases: any;
  public master: any;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams
  ) {
    this.status = navParams.get('status');
    this.title = navParams.get('title');
    this.type = (typeof navParams.get('type') !== 'undefined')? navParams.get('type'): 'J';
    this.master = [
      {type: 'J', status: 'open', number: '1236438', subject: 'Separación de bienes', against: 'Sra. Amaranta Herrera', assigned: 'Dr. José Gaviria', open: '2018-03-02', updated: '2018-04-01', unreads: 1},
      {type: 'J', status: 'open', number: '6546598', subject: 'Situación de bienes', against: 'Sra. Amaranta Herrera', assigned: 'Dr. José Gaviria', open: '2018-03-02', updated: '2018-04-01', unreads: 0},
      {type: 'J', status: 'open', number: '3216513', subject: 'Embargo preventivo', against: 'Sra. Amaranta Herrera', assigned: 'Dr. José Gaviria', open: '2018-03-02', updated: '2018-04-01', unreads: 1},
      {type: 'J', status: 'open', number: '952165165', subject: 'Permiso de viaje', against: 'Sra. Amaranta Herrera', assigned: 'Dr. José Gaviria', open: '2018-03-02', updated: '2018-04-01', unreads: 0},
      {type: 'J', status: 'open', number: '285165165', subject: 'Custodia de menores', against: 'Sra. Amaranta Herrera', assigned: 'Dr. José Gaviria', open: '2018-03-02', updated: '2018-04-01', unreads: 0},
      {type: 'J', status: 'open', number: '159159162', subject: 'Divorcio consentido', against: 'Sra. Amaranta Herrera', assigned: 'Dr. José Gaviria', open: '2018-03-02', updated: '2018-04-01', unreads: 0},
      {type: 'C', status: 'open', number: '1236438', subject: 'Consulta impuestos', assigned: 'Lic. María Hernández', open: '2018-03-02', updated: '2018-04-01', unreads: 1},
      {type: 'C', status: 'open', number: '6546598', subject: 'Trámite', assigned: 'Lic. María Hernández', open: '2018-03-02', updated: '2018-04-01', unreads: 0},
      {type: 'C', status: 'open', number: '3216513', subject: 'Trámite', assigned: 'Lic. María Hernández', open: '2018-03-02', updated: '2018-04-01', unreads: 1},
      {type: 'C', status: 'open', number: '952165165', subject: 'Declaración', assigned: 'Lic. María Hernández', open: '2018-03-02', updated: '2018-04-01', unreads: 0},
    ];
    this.changeType(this.type);
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad CasesPage', this.navParams);
  }
  
  changeType(type){
    this.cases = this.master.filter(cases=> cases.type === type);
    console.log('Cambio de tipo', type);
  }

  detail(item){
    this.navCtrl.push('CasePage',{case: item, status: this.status});
    console.log('Detalles del caso', item);
  }

  selectImage(type){
    if(this.type == 'C' && type=='juridical' || this.type == 'J' && type=='accounting') return 'assets/imgs/'+type+'-icon.png';
    return 'assets/imgs/'+type+'-icon-light.png'
  }

  position(){
    if(this.type == 'C') return 'Contador';
    return 'Abogado';
  }
}
