import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-request',
  templateUrl: 'request.html',
})
export class RequestPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RequestPage');
  }

  juridico(){
    console.log("Jurídico");
    this.navCtrl.push('NewJuridicalPage');
  }
  
  contable(){
    console.log("Contable");
    this.navCtrl.push('NewAccountingPage');
  }
}
