import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OkAccountingPage } from './ok-accounting';

@NgModule({
  declarations: [
    OkAccountingPage,
  ],
  imports: [
    IonicPageModule.forChild(OkAccountingPage),
  ],
})
export class OkAccountingPageModule {}
