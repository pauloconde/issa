import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-ok-accounting',
  templateUrl: 'ok-accounting.html',
})
export class OkAccountingPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OkAccountingPage');
  }

  goHome(){
    this.navCtrl.push('HomePage');
    console.log('Ir a inicio');
  }

  viewOpen(){
    this.navCtrl.push('CasesPage', { status: 'open', title: 'Abiertos' });
    console.log('Ver casos abiertos');
  }
}
