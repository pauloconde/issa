import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { JuridicalPage } from './juridical';

@NgModule({
  declarations: [
    JuridicalPage,
  ],
  imports: [
    IonicPageModule.forChild(JuridicalPage),
  ],
})
export class JuridicalPageModule {}
