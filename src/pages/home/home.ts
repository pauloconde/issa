import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  services(){
    this.navCtrl.push('RequestPage');
    console.log('Solicitar servicios');
  }
  
  cases(){
    this.navCtrl.push('CasesPage', { status: 'open', title: 'Abiertos' });
    console.log('Casos abiertos');
  }
  
  faqs(){
    this.navCtrl.push('FaqsPage');
    console.log('Preguntas frecuentes');
  }
  
  contact(){
    this.navCtrl.push('ContactPage',{},{direction: 'back'});
    console.log('Contáctenos');
  }

  profile(){
    this.navCtrl.push('ProfilePage',{},{direction: 'back'});
    console.log('Ver perfil');
  }
}
